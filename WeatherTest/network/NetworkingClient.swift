//
//  NetworkingClient.swift
//  TestAlamofire
//
//  Created by apple on 4/6/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import Alamofire



class NetworkingClient {

    //typealias WebServiceResponse = ([[String: Any]]?, Error?) -> Void

    typealias WebServiceResponse = ([String: Any]?, Error?) -> Void
    
    

    func excute(_ url: URL, completion: @escaping WebServiceResponse) {
            Alamofire.request(url).validate().responseJSON { (response) in
            if let error = response.error {
                completion(nil, error)
            }
            else if let jsonDic = response.result.value as? [String: Any] {
                completion(jsonDic, nil)
            }
            
            
//            else if let jsonArray = response.result.value as? [[String: Any]] {
//                completion(jsonArray, nil)
//            } else if let jsonDic = response.result.value as? [String: Any] {
//                completion([jsonDic], nil)
//            }
        }

    }
}


