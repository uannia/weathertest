//
//  Weather.swift
//  WeatherTest
//
//  Created by MAC OSX on 4/8/19.
//  Copyright © 2019 MAC OSX. All rights reserved.
//

import UIKit

struct Weather {
    var name: String?
    var country: String?
    var localtime: String?
    var temp_c: Double?
    var text: String?
    //var icon: String?

    init(dictionary: [String: Any]) {

        let location = dictionary["location"] as! [String: Any]
        self.name = location["name"] as? String
        self.country = location["country"] as? String
        self.localtime = location["localtime"] as? String

        let current = dictionary["current"] as! [String: Any]
        self.temp_c = current["temp_c"] as? Double

        let condition = current["condition"] as! [String: Any]
        self.text = condition["text"] as? String


    }

}


