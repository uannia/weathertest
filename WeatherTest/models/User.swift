//
//  User.swift
//  Tinder
//
//  Created by Uan on 1/25/19.
//  Copyright © 2019 Uan. All rights reserved.
//

import Foundation
import UIKit

struct User {
    var name : String?
    var age : Int?
    var profression : String?
    var imageUrl1 : String?
    var imageUrl2: String?
    var imageUrl3: String?
    var uid : String?
    var minSeekAge : Int?
    var maxSeekAge : Int?
    
    init(dictionary: [String : Any]) {
        self.name = dictionary["fullName"] as? String ?? ""
        self.imageUrl1 = dictionary["imageUrl1"] as? String ?? ""
        self.imageUrl2 = dictionary["imageUrl2"] as? String
        self.imageUrl3 = dictionary["imageUrl3"] as? String
        self.uid = dictionary["uid"] as? String ?? ""
        self.age = dictionary["age"] as? Int ?? 0
        self.profression = dictionary["profression"] as? String ?? "Not available"
        self.minSeekAge = dictionary["minSeekAge"] as? Int
        self.maxSeekAge = dictionary["maxSeekAge"] as? Int
        
    }
    
//    func toCardViewModel() -> CardViewModel {
//        let atribuText = NSMutableAttributedString(string: name ?? "", attributes: [.font: UIFont.systemFont(ofSize: 32, weight: .heavy)])
//
//        atribuText.append(NSAttributedString(string: "  \(age ?? 0)", attributes: [.font : UIFont.systemFont(ofSize: 24, weight: .regular)]))
//
//        atribuText.append(NSAttributedString(string: " \n \(profression!)", attributes: [.font : UIFont.systemFont(ofSize: 24, weight: .regular)]))
//
//        var imageUrls = [String]() // empty string array
//        if let url = imageUrl1 { imageUrls.append(url) }
//        if let url = imageUrl2 { imageUrls.append(url) }
//        if let url = imageUrl3 { imageUrls.append(url) }
//
//        return CardViewModel(imageNames: imageUrls, attributeString: atribuText, textAliment: .left)
//    }
    
}


