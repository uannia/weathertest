//
//  WeatherCell.swift
//  WeatherTest
//
//  Created by MAC OSX on 4/9/19.
//  Copyright © 2019 MAC OSX. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var txtName: UILabel!

    @IBOutlet weak var txtCountry: UILabel!
    @IBOutlet weak var txtTime: UILabel!

    @IBOutlet weak var txtTemp: UILabel!

    @IBOutlet weak var txtCon: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        backgroundColor = .red
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }


    public func setData(model: Weather) {
        if let name = model.name {
            txtName?.text = name
        }
        if let name1 = model.country {
            txtCountry.text = name1
        }
        if let name2 = model.localtime {
            txtTime.text = name2
        }
        if let name3 = model.temp_c {
            txtTemp.text = "\(name3) *C"
        }
        if let name4 = model.text {
            txtCon.text = name4
        }

    }




    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
