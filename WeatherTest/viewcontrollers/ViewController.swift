//
//  ViewController.swift
//  WeatherTest
//
//  Created by MAC OSX on 4/8/19.
//  Copyright © 2019 MAC OSX. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {



    let networkingClient = NetworkingClient()
    @IBOutlet weak var tableView: UITableView!
    private var list = [Weather]()
    private let cellId = "cellid"

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchCurrentWeather()
        initTableView()
    }

    private func initTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        //tableView.register(WeatherCell.self, forCellReuseIdentifier: cellId)
        tableView.register( UINib(nibName: "WeatherCell", bundle: Bundle(for: WeatherCell.self)), forCellReuseIdentifier: cellId)
        
    }




    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellId) as? WeatherCell
        let model = list[indexPath.item]
        cell?.setData(model: model)
        return cell!
    }

    private func fetchCurrentWeather() {
        guard let urlRequest = URL(string: AppConst.url) else { return }
        networkingClient.excute(urlRequest) { (json, err) in
            if(err == nil) {
                //success
                if let json1 = json {
                    let model = Weather.init(dictionary: json1)
                    self.list.append(model)
                    self.tableView.reloadData()
                    print(model)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                       self.fetchCurrentWeather()
                    }
                }

            } else {
                // fail
            }
        }
    }

    


}

